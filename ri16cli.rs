use std::io::{prelude::*, stdin, BufReader, Cursor, copy};
use std::fs::{File, write};

#[path = "ri16cpu.rs"]
pub mod cpu;
use cpu::*;

fn main() {
    let mut cpu = CpuState::reset();
    let mut memory = Vec::new();
    memory.resize(MEMORY_SIZE, 0);
    let mut action = Action::None;

    let stdin = stdin();
    let stdin = BufReader::new(stdin.lock());
    for line in stdin.lines() {
        let line = match line {
            Ok(l) => l,
            Err(e) => {
                println!("Input failed: {:?}", e);
                return;
            }
        };

        if let Action::Load { .. } = action {
            let mut radix = 10;
            let mut line = line.as_str();
            if line.starts_with("0x") {
                line = &line[2..];
                radix = 16;
            } else if line.starts_with("0b") {
                line = &line[2..];
                radix = 2
            }
            let value = match u16::from_str_radix(line, radix) {
                Ok(v) => v,
                Err(_) => {
                    println!("Invalid word. Please reply to {:?}", &action);
                    continue;
                },
            };
            cpu.reply(value);
            action = Action::None;
            continue;
        }
        action = Action::None;

        let mut words = line.split_whitespace();

        match words.next() {
            None => (),
            Some("n") | Some("step") => {
                let count = match words.next().map(str::parse::<u32>) {
                    Some(Ok(x)) => x,
                    Some(_) => {
                        println!("Invalid number of steps.");
                        continue;
                    },
                    None => 1,
                };
                for _ in 0..count {
                    action = cpu.step(&mut memory);
                    if action != Action::None {
                        break;
                    }
                }
            },
            Some("p") | Some("play") => {
                while action == Action::None {
                    action = cpu.step(&mut memory);
                }
            },
            Some("r") | Some("reset") => {
                cpu = CpuState::reset();
            },
            Some("d") | Some("debug") => {
                println!("{:#0x?}", &cpu);
            },
            Some("i") | Some("interrupt") => {
                cpu.interrupt(match words.next().map(str::parse::<u16>) {
                    Some(Ok(x)) => x,
                    _ => {
                        println!("Invalid arg for interrupt.");
                        continue;
                    },
                });
            },
            Some("l") | Some("load") => {
                let mut file = match words.next().map(File::open) {
                    Some(Ok(f)) => f,
                    Some(Err(e)) => {
                        println!("Could not open file: {:?}", e);
                        continue;
                    },
                    None => {
                        println!("No file given.");
                        continue;
                    },
                };
                if let Err(e) = copy(
                    &mut file,
                    &mut Cursor::new(mem_as_bytes(&mut memory)),
                ) {
                    println!("Could not load memory from file: {:?}", e);
                }
            },
            Some("s") | Some("store") => {
                let path = match words.next() {
                    Some(p) => p,
                    None => {
                        println!("No file given.");
                        continue;
                    },
                };
                if let Err(e) = write(
                    path,
                    mem_as_bytes(&mut memory),
                ) {
                    println!("Could not save memory into file: {:?}", e);
                }
            },
            Some(c) => {
                println!("Unknown command {}. \
                Try one of step, play, reset, debug, interrupt, load, or store.", c);
            }
        }

        if action != Action::None {
            println!("{:?}", &action);
        }
    }

}
