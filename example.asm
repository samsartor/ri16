:start # program starts here
value jmp 0 # disable interrupts
# setup stack ptr
value next 15
*lbl :stack

# setup interrupt handler
value next 0
*lbl :interrupt
value handler 0

value next 8
*lbl :main # address of :main
:start_loop
value jmp 8
ljmp # run main
value max 0
jnz 0 :start_loop

:jmp_restore
load 0 0 1
value jmp 0
ljmp

:interrupt # root interrupt handler
value jmp 0 # disable interrupts
# assume data is stored in the 8 words above the stack addr
inc 15 8

# save the 8 registers that the interrupte may not have saved
store 0 15 0
store 1 15 1
store 2 15 2
store 3 15 3
store 4 15 4
store 5 15 5
store 6 15 6
store 7 15 7

# save the return addr
value return 0
store 0 15 8

# get the argument
value arg 1

# call the indexed interrupt handler
value next 7
*word 0x00ff
nand 2 1 7
nand 2 2 2 # first byte in index
value next 7
*lbl :interrupt_table
add 2 2 7 # ptr to the handler addr
load 2 2 0 # handler addr

inc 15 9
value jmp 2
ljmp # call the indexed handler
dec 15 9

# reload most registers
load 1 15 1
load 2 15 2
load 3 15 3
load 4 15 4
load 5 15 5
load 6 15 6
load 7 15 7

# set the jmp register and disable further interrupts
load 0 15 8
value jmp 0
load 0 15 0

# exit the root handler
dec 15 8
ljmp

# FUNCTION SECTION

:main
value return 0
# Do stuff
value next 1
*word 0xf000
value next 2
*word 1337
store 2 1
# Go back to loop
value jmp 0
ljmp

:on_beef
value return 0
value next 1
*word 0xf000
value next 2
*word 0xbeef
store 2 1 9
value jmp 0
ljmp

:on_dbg
value return 0
value next 1
*word 0xf000
value pc 2
store 2 1 7 # pc
store 0 1 8 # return
store 15 1 9 # stack
value jmp 0
ljmp

# DATA SECTION

:interrupt_table
*lbl :on_beef
*lbl :on_dbg
*word 0xf000

:stack

