pub type Reg = usize;
pub type Memory = [u16];
pub const MEMORY_SIZE: usize = 0xf000;
pub const INTERRUPT_QUEUE_SIZE: usize = 4;

pub fn mem_as_bytes(mem: &mut Memory) -> &mut [u8] {
    unsafe {
        core::slice::from_raw_parts_mut(
            mem.as_ptr() as *mut u8,
            mem.len() * 2,
        )
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Instruction {
    Sleep,
    Ljmp,
    Inc(Reg, u8),
    Dec(Reg, u8),
    Load(Reg, Reg, u8),
    Store(Reg, Reg, u8),
    Value(SpecialRegister, Reg),
    Jnz(Reg, i8),
    Nand(Reg, Reg, Reg),
    Add(Reg, Reg, Reg),
    Shift(Reg, Reg, Reg),
    Mul(Reg, Reg, Reg),
    Div(Reg, Reg, Reg),
}

impl Instruction {
    pub fn nop() -> Self {
        Instruction::Value(SpecialRegister::Nop, 0)
    }

    pub fn decode(x: u16) -> Self {
        use self::Instruction::*;

        let op = ((x >> 0) & 0x0f) as Reg;
        let a = ((x >> 4) & 0x0f) as Reg;
        let b = ((x >> 8) & 0x0f) as Reg;
        let c = ((x >> 12) & 0x0f) as Reg;
        let i = ((x >> 8) & 0xff) as u8;
        let ih = c as u8;

        match op {
            0 => Sleep,
            1 => Ljmp,
            2 => Inc(a, i),
            3 => Dec(a, i),
            4 => Load(a, b, ih),
            5 => Store(a, b, ih),
            6 => Value(SpecialRegister::decode(a), b),
            7 => Jnz(a, i8::from_le_bytes([i])),
            8 => Nand(a, b, c),
            9 => Add(a, b, c),
            10 => Shift(a, b, c),
            11 => Mul(a, b, c),
            12 => Div(a, b, c),
            _ => Instruction::nop(),
        }
    }

    pub fn encode(self) -> u16 {
        use self::Instruction::*;

        let [c, b, a, op]: [usize; 4] = match self {
            Sleep => [0, 0, 0, 0],
            Ljmp => [0, 0, 0, 1],
            Inc(a, b) => [0, b as usize, a, 2],
            Dec(a, b) => [0, b as usize, a, 3],
            Load(a, b, c) => [c as usize, b, a, 4],
            Store(a, b, c) => [c as usize, b, a, 5],
            Value(a, b) => [0, b, a.encode(), 6],
            Jnz(a, b) => [0, b.to_le_bytes()[0] as usize, a, 7],
            Nand(a, b, c) => [c, b, a, 8],
            Add(a, b, c) => [c, b, a, 9],
            Shift(a, b, c) => [c, b, a, 10],
            Mul(a, b, c) => [c, b, a, 11],
            Div(a, b, c) => [c, b, a, 12],
        };
        (op | (a << 4) | (b << 8) | (c << 12)) as u16
    }
}

impl Default for Instruction {
    fn default() -> Self {
        Instruction::nop()
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecialRegister {
    Zero,
    Max,
    Next,
    Pc,
    Jmp,
    Ret,
    Overflow,
    Handler,
    Arg,
    Segment,
    Nop,
}

impl SpecialRegister {
    pub fn decode(reg: Reg) -> Self {
        use self::SpecialRegister::*;

        match reg {
            0 => Zero,
            1 => Max,
            2 => Next,
            3 => Pc,
            4 => Jmp,
            5 => Ret,
            6 => Overflow,
            7 => Handler,
            8 => Arg,
            9 => Segment,
            _ => Nop,
        }
    }

    pub fn encode(self) -> Reg {
        use self::SpecialRegister::*;

        match self {
            Zero => 0,
            Max => 1,
            Next => 2,
            Pc => 3,
            Jmp => 4,
            Ret => 5,
            Overflow => 6,
            Handler => 7,
            Arg => 8,
            Segment => 9,
            Nop => 15,
        }
    }
}
