use std::mem::ManuallyDrop;
use std::collections::HashMap;
use std::str::SplitWhitespace;
use std::convert::TryInto;

#[path = "ri16isa.rs"]
pub mod isa;
use isa::*;

#[derive(Debug)]
pub enum Expected<'a> {
    LineEnd(&'a str),
    Instruction(&'a str),
    Register(&'a str),
    SpecialRegister(&'a str),
    Label(&'a str),
    NearbyLabel(&'a str),
    Immediate(&'a str, usize),
}

impl<'a> Expected<'a> {
    pub fn found(&self) -> &'a str {
        use Expected::*;

        match self {
            LineEnd(s) => s,
            Instruction(s) => s,
            Register(s) => s,
            SpecialRegister(s) => s,
            Label(s) => s,
            NearbyLabel(s) => s,
            Immediate(s, _) => s,
        }
    }

    pub fn offset_in(&self, s: &'a str) -> usize {
        (self.found().as_ptr() as usize).saturating_sub(s.as_ptr() as usize)
    }
}

struct Args<'a> {
    iter: SplitWhitespace<'a>,
    pub end: &'a str,
}

impl<'a> Args<'a> {
    fn new(line: &'a str) -> Self {
        Args {
            iter: line.split_whitespace(),
            end: &line[line.len()..],
        }
    }

    fn next(&mut self) -> Option<&'a str> {
        let text = self.iter.next()?;
        if text.starts_with('#') {
            self.end = &text[0..0];
            None
        } else {
            Some(text)
        }
    }
}

fn next_end<'a, T>(x: T, args: &mut Args<'a>) -> Result<T, Expected<'a>> {
    match args.next() {
        None => Ok(x),
        Some(e) => Err(Expected::LineEnd(e)),
    }
}

fn next_reg<'a>(args: &mut Args<'a>) -> Result<Reg, Expected<'a>> {
    match args.next().map(|x| (x, x.parse::<usize>())) {
        None => Err(Expected::Register(args.end)),
        Some((_, Ok(x))) if x < 16 => Ok(x),
        Some((x, _)) => Err(Expected::Register(x)),
    }
}

fn next_special_reg<'a>(args: &mut Args<'a>)
    -> Result<SpecialRegister, Expected<'a>>
{
    use SpecialRegister::*;

    match args.next() {
        None => Err(Expected::SpecialRegister(args.end)),
        Some("zero") => Ok(Zero),
        Some("max") => Ok(Max),
        Some("next") => Ok(Next),
        Some("pc") => Ok(Pc),
        Some("jump") | Some("jmp") => Ok(Jmp),
        Some("return") | Some("ret") => Ok(Ret),
        Some("overflow") | Some("over") => Ok(Overflow),
        Some("handler") | Some("hand") => Ok(Handler),
        Some("argument") | Some("arg") => Ok(Arg),
        Some("segment") | Some("seg") => Ok(Segment),
        Some(x) => Err(Expected::SpecialRegister(x)),
    }
}

fn next_label<'a>(args: &mut Args<'a>, labels: &HashMap<&'a str, usize>)
    -> Result<usize, Expected<'a>>
{
    let label = match args.next() {
        None => Err(Expected::Label(args.end)),
        Some(x) if x.starts_with(':') => Ok(&x[1..]),
        Some(x) => Err(Expected::Label(x)),
    }?;

    labels.get(label).copied().ok_or(Expected::Label(label))
}

fn next_label_offset<'a>(
    args: &mut Args<'a>,
    addr: usize,
    labels: &HashMap<&'a str, usize>,
)
    -> Result<i8, Expected<'a>>
{
    let label = next_label(args, labels)? as isize;
    (label - addr as isize)
        .try_into()
        .map_err(|_| Expected::NearbyLabel(args.end))
}

fn next_immediate<'a>(args: &mut Args<'a>, mut bits: usize, optional: bool)
    -> Result<isize, Expected<'a>>
{
    let mut i = match args.next() {
        None if optional => Ok("0"),
        None => Err(Expected::Immediate(args.end, bits)),
        Some(x) => Ok(x),
    }?;

    let neg = i.starts_with('-');
    if neg {
        i = &i[1..];
    }

    let radix = if i.starts_with("0x") {
        i = &i[2..];
        16
    } else if i.starts_with("0b") {
        i = &i[2..];
        2
    } else {
        10
    };

    let mut x = u32::from_str_radix(i, radix)
        .map_err(|_| Expected::Immediate(i, bits))? as isize;
    if neg {
        bits = bits.saturating_sub(1);
    }
    if x >= 1 << bits {
        return Err(Expected::Immediate(i, bits));
    }
    if neg {
        x = -x;
    }
    Ok(x)
}

fn next_immediate_u16<'a>(args: &mut Args<'a>, optional: bool) -> Result<u16, Expected<'a>> {
    let bytes = next_immediate(args, 16, optional)?.to_le_bytes();
    Ok(u16::from_le_bytes([bytes[0], bytes[1]]))
}

fn next_immediate_u8<'a>(args: &mut Args<'a>, optional: bool) -> Result<u8, Expected<'a>> {
    let bytes = next_immediate(args, 8, optional)?.to_le_bytes();
    Ok(bytes[0])
}

fn next_immediate_u4<'a>(args: &mut Args<'a>, optional: bool) -> Result<u8, Expected<'a>> {
    let bytes = next_immediate(args, 4, optional)?.to_le_bytes();
    Ok(bytes[0])
}

fn next_instruction<'a>(
    args: &mut Args<'a>,
    labels: &HashMap<&'a str, usize>,
    addr: usize,
)
    -> Result<Option<u16>, Expected<'a>>
{
    use Instruction::*;

    let i = match args.next() {
        None => return Ok(None),
        Some(x) if x.starts_with(':') => return Ok(None),
        Some("*word") | Some("*") => return Ok(Some(
            next_end(next_immediate_u16(args, false)?, args)?,
        )),
        Some("*label") | Some("*lbl") => return Ok(Some(
            next_end(next_label(args, labels)? as u16, args)?,
        )),
        Some("sleep") => Sleep,
        Some("ljmp") => Ljmp,
        Some("inc") => Inc(
            next_reg(args)?,
            next_immediate_u8(args, false)?,
        ),
        Some("dec") => Dec(
            next_reg(args)?,
            next_immediate_u8(args, false)?,
        ),
        Some("load") => Load(
            next_reg(args)?,
            next_reg(args)?,
            next_immediate_u4(args, true)?,
        ),
        Some("store") => Store(
            next_reg(args)?,
            next_reg(args)?,
            next_immediate_u4(args, true)?,
        ),
        Some("value") => Value(
            next_special_reg(args)?,
            next_reg(args)?,
        ),
        Some("jnz") => Jnz(
            next_reg(args)?,
            next_label_offset(args, addr, labels)?,
        ),
        Some("nand") => Nand(
            next_reg(args)?,
            next_reg(args)?,
            next_reg(args)?,
        ),
        Some("add") => Add(
            next_reg(args)?,
            next_reg(args)?,
            next_reg(args)?,
        ),
        Some("shift") => Shift(
            next_reg(args)?,
            next_reg(args)?,
            next_reg(args)?,
        ),
        Some("mul") => Mul(
            next_reg(args)?,
            next_reg(args)?,
            next_reg(args)?,
        ),
        Some("div") => Div(
            next_reg(args)?,
            next_reg(args)?,
            next_reg(args)?,
        ),
        Some(x) => return Err(Expected::Instruction(x)),
    };

    next_end((), args)?;
    Ok(Some(i.encode()))
}

pub fn assemble_str<'a>(asm: &'a str) -> Result<Vec<u16>, Expected<'a>> {
    let mut labels = HashMap::new();
    let mut addr = 0;
    for line in asm.lines() {
        let line = line.trim();
        if line.is_empty() || line.starts_with("#") {
        } else if line.starts_with(":") {
            labels.insert(line[1..]
                .split(&[' ', '\t', '#'][..])
                .next()
                .unwrap(),
                addr,
            );
        } else {
            addr += 1;
        }
    }

    let mut bin = Vec::new();
    bin.reserve(addr);

    for line in asm.lines() {
        if let Some(b) = next_instruction(
            &mut Args::new(line),
            &labels,
            bin.len(),
        )? {
            bin.push(b);
        }
    }

    Ok(bin)
}

fn main() {
    use std::io::{prelude::*, stdin, stdout};
    use std::process::exit;

    let mut asm = String::new();
    match stdin().lock().read_to_string(&mut asm) {
        Ok(_) => (),
        Err(e) => {
            eprintln!("could not read stdin as utf8: {:?}", e);
            exit(1)
        },
    }
    match assemble_str(asm.as_str()) {
        Ok(mut bin) => {
            if let Err(e) = stdout().lock().write_all(mem_as_bytes(&mut bin)) {
                eprintln!("could not write to stdout: {:?}", e);
                exit(1);
            }
        },
        Err(e) => {
            eprintln!(
                "expected {:?} at position {}",
                e,
                e.offset_in(asm.as_str()),
            );
            exit(1);
        },
    }
}

#[no_mangle]
unsafe extern "C" fn assemble(
    asm: *const u8,
    len: usize,
    bin: *mut *mut u16,
) -> isize {
    let asm = std::slice::from_raw_parts(asm, len);
    let asm = match std::str::from_utf8(asm) {
        Ok(s) => s,
        Err(_) => return -(asm.as_ptr() as isize),
    };
    let mut out = ManuallyDrop::new(match assemble_str(asm) {
        Ok(b) => b,
        Err(e) => return -(e.found().as_ptr() as isize),
    });
    *bin = out.as_mut_ptr();
    out.len() as isize
}
