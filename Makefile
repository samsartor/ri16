example: example.asm ri16asm
	cat example.asm | ./ri16asm > example

ri16asm: ri16asm.rs ri16isa.rs
	rustc ri16asm.rs -o ri16asm -C opt-level=3

ri16cpu.so: ri16cpu.rs ri16isa.rs
	rustc ri16cpu.rs -o ri16cpu.so -C opt-level=z -C panic=abort --crate-type=cdylib

ri16cli: ri16cli.rs ri16cpu.rs ri16isa.rs
	rustc ri16cli.rs -o ri16cli -C opt-level=3

all: example ri16cli ri16cpu.so
