#include "stdint.h"
#include "stddef.h"

typedef enum {
  RI16CPU_ACTION_NONE = 0,
  RI16CPU_ACTION_YIELD = 1,
  RI16CPU_ACTION_LOAD = 2,
  RI16CPU_ACTION_STORE = 3,
} Ri16Cpu_Action;

size_t ri16cpu_cpu_size();

void ri16cpu_cpu_reset(void *cpu);

uint16_t *ri16cpu_cpu_regs(void *cpu);

Ri16Cpu_Action ri16cpu_step(
  void *cpu,
  uint16_t *mem,
  size_t mem_size,
  uint16_t *addr_out,
  uint16_t *seg_out,
  uint16_t *value_out);

void ri16cpu_reply(void *cpu, uint16_t value);

void ri16cpu_interrupt(void *cpu, uint16_t value);
