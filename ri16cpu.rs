#[path = "ri16isa.rs"]
pub mod isa;
pub use isa::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Action {
    None,
    Yield,
    Load {
        addr: u16,
        seg: u16,
    },
    Store {
        value: u16,
        addr: u16,
        seg: u16,
    },
}

pub fn load(mem: &mut Memory, seg: u16, addr: u16) -> Result<u16, Action> {
    if addr < MEMORY_SIZE as u16 {
        Ok(mem.get(addr as usize).copied().unwrap_or(0))
    } else {
        Err(Action::Load { addr, seg })
    }
}

pub fn store(mem: &mut Memory, seg: u16, addr: u16, value: u16) -> Result<(), Action> {
    if addr < MEMORY_SIZE as u16 {
        if let Some(x) = mem.get_mut(addr as usize) {
            *x = value;
        }
        Ok(())
    } else {
        Err(Action::Store { value, addr, seg })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum CpuMode {
    Ready,
    PendingNext,
    PendingReg(Reg),
    PendingInterrupt,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum InterruptMode {
    Disabled,
    DisabledWeak,
    Enabled,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CpuState {
    mode: CpuMode,
    pub regs: [u16; 16],
    next: u16,
    pc: u16,
    jmp: u16,
    int: InterruptMode,
    ret: u16,
    handler: u16,
    overflow: bool,
    arg: u16,
    segment: u16,
    queue: [u16; INTERRUPT_QUEUE_SIZE],
    queue_len: usize,
}

impl CpuState {
    pub fn reset() -> Self {
        CpuState {
            mode: CpuMode::Ready,
            regs: [0; 16],
            next: 0,
            pc: MEMORY_SIZE as u16,
            jmp: 0,
            int: InterruptMode::Enabled,
            ret: 0,
            handler: MEMORY_SIZE as u16,
            overflow: false,
            arg: 0,
            segment: 0,
            queue: [0; INTERRUPT_QUEUE_SIZE],
            queue_len: 1, // Conceptually, reset into interrupt
        }
    }

    pub fn reset_then(i: Instruction, mem: &mut Memory) -> Self {
        let mut cpu = self::CpuState::reset();
        assert_eq!(cpu.step(mem), Action::Load {
            addr: MEMORY_SIZE as u16,
            seg: 0,
        });
        cpu.reply(i.encode());
        cpu
    }

    pub fn value(&mut self, of: SpecialRegister, reg: &mut u16) {
        use self::SpecialRegister::*;

        match of {
            Zero => *reg = 0,
            Max => *reg = core::u16::MAX,
            Next => {
                *reg = self.next;
                self.next = Instruction::nop().encode();
            },
            Pc => *reg = self.pc,
            Jmp => {
                self.jmp = *reg;
                self.int = InterruptMode::Disabled;
            },
            Ret => *reg = self.ret,
            Handler => self.handler = *reg,
            Overflow => *reg = self.overflow as u16,
            Arg => *reg = self.arg,
            Segment => self.segment = *reg,
            Nop => (),
        }
    }

    pub fn step(&mut self, mem: &mut Memory) -> Action {
        use self::CpuMode::*;
        use self::Instruction::*;
        use self::InterruptMode::*;

        match (self.int, self.mode) {
            (Enabled, Ready) if self.queue_len > 0 => {
                self.arg = self.pop_queue();
                self.int = DisabledWeak;
                self.mode = Ready;
                self.ret = self.pc;
                self.pc = self.handler;
                return match load(mem, self.segment, self.handler) {
                    Ok(x) => {
                        self.next = x;
                        Action::None
                    },
                    Err(a) => {
                        self.mode = PendingNext;
                        a
                    },
                }
            },
            (Enabled, PendingInterrupt) if self.queue_len > 0 => {
                self.arg = self.pop_queue();
                self.int = DisabledWeak;
                self.mode = Ready;
            },
            (DisabledWeak, Ready) => self.int = Enabled,
            (_, Ready) => (),
            _ => return Action::Yield,
        }

        let i = Instruction::decode(self.next);

        let mut next_addr = if i == Instruction::Ljmp {
            self.jmp
        } else if i == Instruction::Sleep {
            self.handler
        } else {
            self.pc + 1
        };
        let mut action = Action::None;

        match load(mem, self.segment, next_addr) {
            Ok(x) => self.next = x,
            Err(a) => {
                self.mode = PendingNext;
                action = a;
            },
        };

        match i {
            Sleep => {
                self.int = Enabled;
                self.mode = PendingInterrupt;
                self.ret = self.pc + 1;
                if action == Action::None {
                    action = Action::Yield;
                }
            },
            Ljmp => {
                self.int = DisabledWeak;
                self.ret = self.pc + 1;
            },
            Inc(r, x) => {
                let x = self.regs[r].overflowing_add(x as u16);
                self.regs[r] = x.0;
                self.overflow = x.1;
            },
            Dec(r, x) => {
                let x = self.regs[r].overflowing_sub(x as u16);
                self.regs[r] = x.0;
                self.overflow = x.1;
            },
            Load(r, a, i) => match load(mem,
                self.segment,
                self.regs[a] + i as u16,
            ) {
                Ok(x) => self.regs[r] = x,
                Err(a) => {
                    // FIXME: overrides PendingNext
                    self.mode = PendingReg(r);
                    action = a;
                }
            },
            Store(r, a, i) => if let Err(a) = store(
                mem,
                self.segment,
                self.regs[a] + i as u16,
                self.regs[r],
            ) {
                // FIXME: overrides PendingNext
                action = a;
            },
            Value(s, r) => {
                let mut v = self.regs[r];
                self.value(s, &mut v);
                self.regs[r] = v;
                if self.mode == PendingNext && s == SpecialRegister::Next {
                    self.mode = PendingReg(r);
                }
            },
            Jnz(r, i) => if self.regs[r] != 0 {
                if i >= 0 {
                    next_addr = self.pc.saturating_add(i as u16);
                } else {
                    next_addr = self.pc.saturating_sub((-i) as u16);
                }
                match load(mem, self.segment, next_addr) {
                    Ok(x) => self.next = x,
                    Err(a) => {
                        // FIXME: overrides PendingNext
                        self.mode = PendingNext;
                        action = a;
                    },
                };
            },
            Nand(o, a, b) => self.regs[o] = !(self.regs[a] & self.regs[b]),
            Add(o, a, b) => {
                let x = self.regs[a].overflowing_add(self.regs[b]);
                self.regs[o] = x.0;
                self.overflow = x.1;
            },
            Shift(o, a, b) => self.regs[o] = match self.regs[b] {
                b if b <= 16 => self.regs[a] << b,
                // Extra conversion to do sign extension
                b => u16::from_le_bytes(i16::to_le_bytes(
                    i16::from_le_bytes(self.regs[a].to_le_bytes()) >> b,
                )),
            },
            Mul(o, a, b) => {
                let x = self.regs[a].overflowing_mul(self.regs[b]);
                self.regs[o] = x.0;
                self.overflow = x.1;
            },
            Div(o, a, b) => match self.regs[b] {
                0 => {
                    self.regs[o] = core::u16::MAX;
                    self.overflow = true;
                },
                b => {
                    let x = self.regs[a].overflowing_div(b);
                    self.regs[o] = x.0;
                    self.overflow = x.1;
                },
            }
        };

        self.pc = next_addr;
        action
    }

    fn pop_queue(&mut self) -> u16 {
        self.queue_len -= 1;
        let old = self.queue;
        for (a, b) in self.queue[..INTERRUPT_QUEUE_SIZE - 1]
            .iter_mut()
            .zip(&old[1..])
        {
            *a = *b;
        }
        old[0]
    }

    pub fn interrupt(&mut self, arg: u16) {
        if self.queue_len == INTERRUPT_QUEUE_SIZE {
            self.pop_queue();
        }
        self.queue[self.queue_len] = arg;
        self.queue_len += 1;
    }

    pub fn reply(&mut self, mem: u16) {
        use self::CpuMode::*;

        match self.mode {
            Ready | PendingInterrupt => return,
            PendingNext => self.next = mem,
            PendingReg(r) => self.regs[r] = mem,
        }

        self.mode = Ready;
    }
}

#[no_mangle]
extern "C" fn ri16cpu_cpu_size() -> usize {
    core::mem::size_of::<CpuState>()
}

#[no_mangle]
extern "C" fn ri16cpu_cpu_reset(cpu: &mut core::mem::MaybeUninit<CpuState>) {
    *cpu = core::mem::MaybeUninit::new(CpuState::reset());
}

#[no_mangle]
extern "C" fn ri16cpu_cpu_regs(cpu: &mut CpuState) -> *mut u16 {
    cpu.regs[..].as_mut_ptr()
}

#[no_mangle]
unsafe extern "C" fn ri16cpu_step(
    cpu: &mut CpuState,
    mem: *mut u16,
    mem_size: usize,
    addr_ptr: *mut u16,
    seg_ptr: *mut u16,
    value_ptr: *mut u16,
) -> u32 {
    let mem = core::slice::from_raw_parts_mut(mem, mem_size);
    match cpu.step(mem) {
        Action::None => 0,
        Action::Yield => 1,
        Action::Load { addr, seg } => {
            *addr_ptr = addr;
            *seg_ptr = seg;
            2
        },
        Action::Store { addr, seg, value} => {
            *addr_ptr = addr;
            *seg_ptr = seg;
            *value_ptr = value;
            3
        },
    }
}

#[no_mangle]
extern "C" fn ri16cpu_reply(cpu: &mut CpuState, value: u16) {
    cpu.reply(value);
}

#[no_mangle]
extern "C" fn ri16cpu_interrupt(cpu: &mut CpuState, value: u16) {
    cpu.interrupt(value);
}
